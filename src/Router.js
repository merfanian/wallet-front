import React, {Component} from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import Login from '../src/components/SignInSide';
import Home from '../src/components/LandingPage';
import SignupSlide from '../src/components/SignupSlide';
import ProfilePage from '../src/components/ProfilePage';
import Dashboard from './components/Dashboard';

const PrivateRoute = ({ component: Component, isAuth, token, email, handleAuth, ...rest }) => (
  <Route {...rest} render={(props) => (
    isAuth
      ? <Component {...props} token={token} email={email} handleAuth={handleAuth} />
      : <Redirect to='/login' />
  )} />
)
	
const PrivateRoute2 = ({ component: Component, isAuth, handleToken, handleAuth, handleEmail, ...rest }) => (
  <Route {...rest} render={(props) => (
    isAuth
      ? <Redirect to='/dashboard' />
      : <Component {...props} handleToken={handleToken} handleAuth={handleAuth} handleEmail={handleEmail} />
  )} />
)
	
class Main extends Component {
	constructor(props) {
        super(props);
		this.handleAuth = this.handleAuth.bind(this);
		this.handleToken = this.handleToken.bind(this);
		this.handleEmail = this.handleEmail.bind(this);
		
        this.state = {
            isAuth: false,
            token: '',
			email: '',
        };
    }
	
	handleAuth(auth) {
		this.setState({isAuth: auth});
		this.forceUpdate();
	}
	
	handleToken(tok) {
		this.setState({token: tok});
		this.forceUpdate()
	}
	
	handleEmail(em) {
		this.setState({email: em});
		this.forceUpdate()
	}
	
	render() {
		return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Home}></Route>
                <PrivateRoute2 exact path="/login" component={Login} isAuth={this.state.isAuth} handleToken={this.handleToken} handleAuth={this.handleAuth} handleEmail={this.handleEmail}></PrivateRoute2>
                <Route exact path="/signup" component={SignupSlide}></Route>
				<PrivateRoute exact path="/dashboard" component={Dashboard} isAuth={this.state.isAuth} token={this.state.token} email={this.state.email} handleAuth={this.handleAuth}></PrivateRoute>
            </Switch>
        </BrowserRouter>
    );
	}
}


export default Main;
