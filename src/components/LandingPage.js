import React from 'react';
import logo from './../logo.png';
import Button from '@material-ui/core/Button';

import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

import './../App.css';

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: 'url(https://source.unsplash.com/random)',
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light'
                ? theme.palette.grey[50]
                : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

function handleLogin(e) {
    window.location.href = '/login';
}

function handleSignup(e) {
    window.location.href = '/signup';
}

function handleProfile(e) {
    window.location.href = '/profile';
}

function LandingPage() {
    const classes = useStyles();

    return (
        <div className="App">
            <header className="App-header">
                <h1 className={classes.paper}>Welcome to Your Wallet</h1>
                <img src={logo} className="App-logo" alt="logo" />
                <div>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={(e) => handleLogin(e)}
                    >
                        Sign In
                    </Button>
                    &nbsp;&nbsp;&nbsp;
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={(e) => handleSignup(e)}
                    >
                        Sign Up
                    </Button>
                </div>
            </header>
        </div>
    );
}

export default LandingPage;
