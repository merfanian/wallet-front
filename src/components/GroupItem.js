import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import InboxIcon from '@material-ui/icons/Inbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import Badge from '@material-ui/core/Badge';
import urls from './Urls';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
}));

class GroupItem extends Component {
    constructor(props) {
        super(props);
        this.handleListItemClick = this.handleListItemClick.bind(this);
    }

    handleListItemClick = (e) => {
        this.props.handleGroupClick(this, this.props.groups);
        this.forceUpdate();
    };

    render() {
        const src = this.props.avatar
            ? urls.avatarPreviewUrl + this.props.avatar
            : 'https://eu.ui-avatars.com/api/?uppercase=false&name=' +
              this.props.title +
              '+' +
              this.props.title +
              '&rounded=true&size=48';
        return (
            <ListItem
                button
                onClick={(e) => this.handleListItemClick(e)}
                selected={this.props.selected}
            >
                <ListItemIcon>
                    <Badge badgeContent={this.props.counter} color="primary">
                        <img
                            src={src}
                            style={{
                                height: '48px',
                                width: '48px',
                                borderRadius: '24px',
                            }}
                        ></img>
                    </Badge>
                </ListItemIcon>
                <ListItemText
                    style={{ color: this.props.color }}
                    primary={this.props.title}
                />
            </ListItem>
        );
    }
}

export default GroupItem;
