import React, { Component } from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import GroupsBar from './GroupsBar';
import MembersList from './MembersList';
import axios from 'axios';
import urls from './Urls';
import { css } from '@emotion/core';
import MenuIcon from '@material-ui/icons/Menu';
import CreateGroupDialog from './CreateGroupDialog';
import UpdateProfileForm from './UpdateProfileForm';
import Review from './Review';
import ReviewPack from './ReviewPack';
import ExitToAppRoundedIcon from '@material-ui/icons/ExitToAppRounded';
import ClearAllRoundedIcon from '@material-ui/icons/ClearAllRounded';
import InviteUserForm from './InviteUserForm';
import 'react-chat-elements/dist/main.css';
import { MessageBox } from 'react-chat-elements';
import { ChatItem } from 'react-chat-elements';
// import { ChatList } from 'react-chat-elements';
import { MessageList } from 'react-chat-elements';
import SockJS from 'sockjs-client/dist/sockjs.js';
import { Client } from '@stomp/stompjs';
import TextField from '@material-ui/core/TextField';
import SendIcon from '@material-ui/icons/Send';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import ScrollToBottom from 'react-scroll-to-bottom';
import Avatar from '@material-ui/core/Avatar';
import Switch from '@material-ui/core/Switch';
import ChatList from './ChatList';
import Stomp from '@stomp/stompjs';
import ChatMessage from './ChatMessage';
import 'emoji-mart/css/emoji-mart.css';
import { Picker } from 'emoji-mart';
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';
import { store } from 'react-notifications-component';

const drawerWidth = 360;

const styles = (theme) => ({
    root: {
        display: 'flex',
        backgroundColor: '#F5F4F2',
    },
    rootDark: {
        display: 'flex',
        backgroundColor: '#34414e',
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    textin: {
        color: 'white',
    },
    textinDark: {
        color: 'white',
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        color: 'white',
        ...theme.mixins.toolbar,
    },
    toolbarIconDark: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        color: 'white',
        ...theme.mixins.toolbar,
    },
    rightLoadingText: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '0 8px',
        color: 'white',
        ...theme.mixins.toolbar,
    },

    leftLoadingText: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '0 8px',
        color: 'white',
        ...theme.mixins.toolbar,
    },
    leftLoadingTextDark: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '0 8px',
        color: 'white',
        ...theme.mixins.toolbar,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        backgroundColor: '#77848A',
    },
    appBarDark: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        backgroundColor: '#404854',
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
        backgroundColor: '#426B80',
    },
    drawerPaperDark: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
        backgroundColor: '#2a293e',
    },
    leftDrawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
        backgroundColor: '#245066',
    },
    leftDrawerPaperDark: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
        backgroundColor: '#110022',
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9),
        },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    },
    chatList: {
        paddingTop: '8%',
        fontSize: 100,
    },
    chatListDark: {
        color: 'white',
        background: '#0f3b57',
        paddingTop: '8%',
        fontSize: 100,
    },
});

var createdClient = null;

var groups = [];

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.createClient = this.createClient.bind(this);
        this.refreshGroups = this.refreshGroups.bind(this);
        this.refreshGroupMembers = this.refreshGroupMembers.bind(this);
        this.refreshMessages = this.refreshMessages.bind(this);
        this.subscribeToChannels = this.subscribeToChannels.bind(this);

        this.state = {
            darkMode: false,
            open: true,
            leftOpen: false,
            groups: [],
            firstName: '',
            lastName: '',
            token: this.props.token,
            email: this.props.email,
            groupMembers: [],
            selectedGroupId: '',
            groupName: '',
            groupsAreLoading: true,
            membersAreLoading: false,
            selectedGroup: '',
            messages: [],
            createdClient: null,
            message: '',
            subscribed: false,
        };
    }

    intervalID;

    createClient() {
        const socket = () => new SockJS(urls.socketUrl);

        createdClient = new Client({
            webSocketFactory: socket,
            reconnectDelay: 0,
            connectHeaders: {
                login: {},
                passcode: this.state.token,
            },
            heartbeatIncoming: 5000,
            heartbeatOutgoing: 5000,
            debug: (text) => console.log(text),
            onConnect: this.refreshGroups,
            onDisconnect: this.setState({ subscribed: false }),
            // onWebSocketClose,
        });

        createdClient.activate();

        this.setState({ createdClient: createdClient });
        return createdClient;
    }

    subscribeToChannels(groups) {
        console.log('sub');
        if (this.state.subscribed) return;
        // if (this.state.createdClient == null) return;
        if (this.state.groups.length === 0) return;
        for (let i = 0; i < this.state.groups.length; i++) {
            console.log(this.state.groups[i]);
            createdClient.subscribe(
                urls.topicSubscribeUrl + '/' + this.state.groups[i].id,
                this.handleNewMessage,
            );
        }

        createdClient.subscribe(urls.notificationUrl, this.handleNotification);
        this.setState({ subscribed: true });
    }

    handleNewMessage = (e) => {
        var messages = this.state.messages;
        var data = JSON.parse(e.body);
        if (data.gathering != this.state.selectedGroupId) {
            var groups = this.state.groups;
            console.log('group is not selected');

            store.addNotification({
                title: 'Wonderful!',
                message: 'You have a new message!',
                type: 'default',
                insert: 'top',
                container: 'bottom-right',
                animationIn: ['animated', 'fadeIn'],
                animationOut: ['animated', 'fadeOut'],
                dismiss: {
                    duration: 2000,
                    onScreen: true,
                },
            });

            for (var i = 0; i < groups.length; i++) {
                if (groups[i].id === data.gathering) {
                    groups[i].counter += 1;
                    console.log('group found' + groups[i].counter);
                    this.setState({ groups: groups });
                    return;
                }
            }
        } else {
            console.log('group is selected');
            var found = false;
            for (var i = 0; i < messages.length; i++) {
                if (messages[i].id === data.id) {
                    found = true;
                    break;
                }
            }
            data.showMeInRight = data.email === this.state.email;
            if (!found) messages.push(data);
            this.setState({ messages: messages });
            this.forceUpdate();
        }
    };

    handleNotification = (e) => {
        console.log(e);
        store.addNotification({
            title: 'Hey!',
            message: 'You were invited to ' + ' ' + '.',
            type: 'info',
            insert: 'top',
            container: 'bottom-right',
            animationIn: ['animated', 'fadeIn'],
            animationOut: ['animated', 'fadeOut'],
            dismiss: {
                duration: 5000,
                onScreen: true,
            },
        });
    };

    handleGroupSelect(group) {
        var groups = this.state.groups;
        for (var i = 0; i < groups.length; i++) {
            if (groups[i].id === group.props.id) {
                groups[i].counter = 0;
                break;
            }
        }

        this.setState({
            selectedGroup: group,
            selectedGroupId: group.props.id,
            groupName: group.props.title,
            membersAreLoading: true,
            leftOpen: true,
            groups: groups,
        });

        this.setState({ selectedGroupId: group.props.id });
        this.refreshGroupMembers(group.props.id);
        this.refreshMessages(group.props.id);
    }

    refreshGroupMembers(groupId) {
        const proxyurl = 'https://cors-anywhere.herokuapp.com/';
        // if (this.state.selectedGroupId === '') return;
        if (groupId === undefined) groupId = this.state.selectedGroupId;
        axios
            .get(urls.groupUrl + '/members/' + groupId, {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: this.state.token,
                    'Access-Control-Allow-Origin': 'http://127.0.0.1:3000',
                    'Access-Control-Allow-Methods': 'POST',
                    'Access-Control-Allow-Headers':
                        'Content-Type, Authorization',
                },
            })
            .then((res) => {
                this.setState({ groupMembers: res.data });
                this.setState({ membersAreLoading: false });
            })
            .catch((res) => {
                this.setState({ groupMembers: [] });
            });
    }

    refreshGroups() {
        if (this.state === undefined) return;
        const proxyurl = 'https://cors-anywhere.herokuapp.com/';
        axios
            .get(urls.groupUrl + '/getGatherings', {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: this.state.token,
                },
            })
            .then((res) => {
                var groups = res.data.map((group) => ({
                    counter: 0,
                    title: group.title,
                    id: group.id,
                    avatar: group.avatar,
                }));
                console.log(groups);
                this.setState({ groups: groups });
                this.setState({ groupsAreLoading: false });
                this.setState({ subscribed: false });
                groups = res.data;
            });
        this.forceUpdate();
        this.subscribeToChannels(groups);
    }

    refreshMessages(groupId) {
        let datas = [];
        axios
            .get(urls.groupUrl + '/history/' + groupId, {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: this.state.token,
                    'Access-Control-Allow-Origin': 'http://127.0.0.1:3000',
                    'Access-Control-Allow-Methods': 'POST',
                    'Access-Control-Allow-Headers':
                        'Content-Type, Authorization',
                },
            })
            .then((res) => {
                var data = res.data;
                for (var i = 0; i < data.length; i++)
                    data[i].showMeInRight = data[i].email === this.state.email;
                this.setState({ messages: data });
            })
            .catch((res) => {
                console.log(res);
            });
    }

    handleAvatar = () => {};

    componentDidMount() {
        this.createClient();

        this.intervalID = setInterval(
            this.subscribeToChannels.bind(this),
            10000,
        );
    }

    componentWillUnmount() {
        clearInterval(this.intervalID);
    }

    handleSwitch = (event) => {
        this.setState({ darkMode: !this.state.darkMode });
    };

    handleDrawerOpen = () => {
        this.setState({ open: true });
    };

    handleDrawerClose = () => {
        this.setState({ open: false });
    };

    handleLogout = () => {
        this.props.handleAuth(false);
    };

    handleTextChange = (e) => {
        this.setState({ message: e.target.value });
    };

    handleEmojiClick = (e) => {
        this.setState({ message: this.state.message.concat(e.native) });
    };

    handleEnter = (e) => {
        if (e.key === 'Enter') {
            e.preventDefault();
            this.handleSend();
        }
    };

    handleSend = () => {
        var des = urls.sendMessageUrl + '/' + this.state.selectedGroupId;
        createdClient.publish({
            destination: des,
            body: JSON.stringify({ message: this.state.message, type: 'TEXT' }),
        });

        this.setState({ message: '', showPicker: false });
        this.forceUpdate();
    };

    handleSendReview = (fileName, cancel) => {
        console.log(fileName);
        var des = urls.sendMessageUrl + '/' + this.state.selectedGroupId;
        console.log(des);
        createdClient.publish({
            destination: des,
            body: JSON.stringify({ message: fileName, type: 'REVIEW' }),
        });
        this.forceUpdate();
        cancel();
    };
    render() {
        const { classes } = this.props;

        return (
            <div
                className={
                    this.state.darkMode ? classes.rootDark : classes.root
                }
            >
                <CssBaseline />
                <AppBar
                    position="absolute"
                    className={clsx(
                        this.state.darkMode
                            ? classes.appBarDark
                            : classes.appBar,
                        this.state.open && classes.appBarShift,
                    )}
                >
                    <Toolbar className={classes.toolbar}>
                        <IconButton
                            edge="start"
                            color="inherit"
                            aria-label="open drawer"
                            onClick={this.handleDrawerOpen}
                            className={clsx(
                                classes.menuButton,
                                this.state.open && classes.menuButtonHidden,
                            )}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography
                            component="h1"
                            variant="h6"
                            color="inherit"
                            noWrap
                            className={classes.title}
                        >
                            {this.state.selectedGroupId === '' ? (
                                <text>Dashboard</text>
                            ) : (
                                <text>{this.state.groupName}</text>
                            )}
                        </Typography>
                        <Tooltip title="Dark mode">
                            <Switch
                                checked={this.state.darkMode}
                                onChange={this.handleSwitch}
                                color="primary"
                                name="Enable Darkmode"
                                inputProps={{
                                    'aria-label': 'primary checkbox',
                                }}
                            />
                        </Tooltip>
                        <ReviewPack
                            token={this.state.token}
                            func={this.handleSendReview}
							darkMode={this.state.darkMode}
                        ></ReviewPack>
                        <Review token={this.state.token}></Review>
                        <CreateGroupDialog
                            token={this.state.token}
                            refreshGroups={this.refreshGroups.bind(this)}
                        ></CreateGroupDialog>
                        <UpdateProfileForm
                            token={this.state.token}
                        ></UpdateProfileForm>
                        <InviteUserForm
                            token={this.state.token}
                            groupId={this.state.selectedGroupId}
                        ></InviteUserForm>
                        <Tooltip title="Toggle members bar">
                            <IconButton
                                color="inherit"
                                disabled={this.state.selectedGroupId === ''}
                                onClick={() =>
                                    this.setState({
                                        leftOpen: !this.state.leftOpen,
                                    })
                                }
                            >
                                <ClearAllRoundedIcon></ClearAllRoundedIcon>
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Logout">
                            <IconButton
                                color="inherit"
                                onClick={this.handleLogout}
                            >
                                <ExitToAppRoundedIcon></ExitToAppRoundedIcon>
                            </IconButton>
                        </Tooltip>
                    </Toolbar>
                </AppBar>
                <Drawer
                    variant="permanent"
                    classes={{
                        paper: clsx(
                            this.state.darkMode
                                ? classes.drawerPaperDark
                                : classes.drawerPaper,
                            !this.state.open && classes.drawerPaperClose,
                        ),
                    }}
                    open={true}
                >
                    <div
                        className={
                            this.state.darkMode
                                ? classes.toolbarIconDark
                                : classes.toolbarIcon
                        }
                    >
                        Groups
                        <IconButton onClick={this.handleDrawerClose}>
                            <ChevronLeftIcon
                                style={{
                                    color: this.state.darkMode
                                        ? 'white'
                                        : 'black',
                                }}
                            />
                        </IconButton>
                    </div>
                    <Divider />
                    <div>
                        {this.state.groupsAreLoading ? (
                            <div
                                className={
                                    this.state.darkMode
                                        ? classes.leftLoadingTextDark
                                        : classes.leftLoadingText
                                }
                            >
                                {' '}
                                Loading ...
                            </div>
                        ) : (
                            <GroupsBar
                                color={this.state.darkMode ? 'white' : 'white'}
                                groups={this.state.groups}
                                handleGroupSelect={(e) =>
                                    this.handleGroupSelect(e)
                                }
                            ></GroupsBar>
                        )}
                    </div>
                </Drawer>
                <main className={classes.content}>
                    {/* <div className={classes.appBarSpacer}> */}
                    <div>
                        <div style={{ paddingTop: '5%' }}>
                            <ChatList
                                messages={this.state.messages}
                                darkMode={this.state.darkMode}
                            ></ChatList>
                        </div>
                        <br></br>
                        <br></br>
                        <div
                            style={{
                                position: 'absolute',
                                bottom: 60,
                                marginLeft: 30,
                                width: 40,
                                height: 40,
                                borderRadius: '20px',
                                opacity: '50%',
                                background: '#34aeeb',
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}
                        >
                            <SentimentVerySatisfiedIcon
                                onClick={(e) => {
                                    this.setState({
                                        showPicker: !this.state.showPicker,
                                    });
                                }}
                                style={{ margin: 'auto', opacity: '100%' }}
                            ></SentimentVerySatisfiedIcon>
                        </div>
                        <div>
                            <div
                                style={{
                                    width: '80%',
                                    position: 'absolute',
                                    bottom: 0,
                                    marginLeft: '5%',
                                    marginBottom: '5%',
                                }}
                            >
                                {this.state.showPicker ? (
                                    <Picker
                                        title="Pick your emoji…"
                                        emoji="point_up"
                                        style={{
                                            position: 'absolute',
                                            bottom: '20px',
                                            left: '20px',
                                        }}
                                        onSelect={(e) =>
                                            this.handleEmojiClick(e)
                                        }
                                    ></Picker>
                                ) : (
                                    <text></text>
                                )}
                            </div>
                            <div
                                style={{
                                    width: '80%',
                                    position: 'absolute',
                                    flexDirection: 'row-reverse',
                                    bottom: 0,
                                    background: this.state.darkMode
                                        ? '#00626f'
                                        : '#77848A',
                                }}
                            >
                                <TextField
                                    fullWidth
                                    style={{
                                        // float: 'left',
                                        textAlign: 'right',
                                        flex: '1',
                                    }}
                                    value={this.state.message}
                                    id="message"
                                    label="Press enter to send"
                                    name="message"
                                    disabled={this.state.selectedGroupId === ''}
                                    onChange={(e) => this.handleTextChange(e)}
                                    onKeyPress={(e) => this.handleEnter(e)}
                                    InputLabelProps={{
                                        className: this.state.darkMode
                                            ? classes.textinDark
                                            : classes.textin,
                                    }}
                                    InputProps={{
                                        className: this.state.darkMode
                                            ? classes.textinDark
                                            : classes.textin,
                                    }}
                                ></TextField>
                            </div>
                        </div>
                    </div>
                </main>
                <Drawer
                    variant="permanent"
                    classes={{
                        paper: clsx(
                            this.state.darkMode
                                ? classes.leftDrawerPaperDark
                                : classes.leftDrawerPaper,
                            !this.state.leftOpen && classes.drawerPaperClose,
                        ),
                    }}
                    open={this.state.selectedGroupId == ''}
                >
                    <div
                        className={
                            this.state.darkMode
                                ? classes.toolbarIconDark
                                : classes.toolbarIcon
                        }
                    >
                        Group's Members
                        <IconButton onClick={this.handleDrawerClose}>
                            <ChevronLeftIcon />
                        </IconButton>
                    </div>
                    <Divider />
                    <div color="inherit">
                        {this.state.membersAreLoading ? (
                            <div className={classes.rightLoadingText}>
                                Loading ...
                            </div>
                        ) : (
                            <MembersList
                                members={this.state.groupMembers}
                            ></MembersList>
                        )}
                    </div>
                </Drawer>
            </div>
        );
    }
}

export default withStyles(styles, { withTheme: true })(Dashboard);
