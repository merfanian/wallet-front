// // const proxyUrl = 'https://cors-anywhere.herokuapp.com/';
// // const proxyUrl = 'wallet-co.ir:8090/';
// // const proxyUrl = 'localhost:8080/';
// const proxyUrl = '';
// const urls = {
//     defaultUrl: proxyUrl + 'wallet-co.ir/wallet',
//     loginUrl: 'wallet-co.ir/wallet/api/authenticate/login',
//     signupUrl:
//         proxyUrl + 'wallet-co.ir/wallet/api/authenticate/register',
//     socketUrl: 'wallet-co.ir/websocket',
//     groupUrl: proxyUrl + 'wallet-co.ir/wallet/api/gathering',
//     profileUrl: proxyUrl + 'wallet-co.ir/wallet/api/profile',
//     topicSubscribeUrl: '/topic/gathering',
//     sendMessageUrl: '/app/gathering',
//     reviewUrl:proxyUrl + 'wallet-co.ir/wallet/api/review',
//     picPreviewUrl: 'wallet-co.ir/wallet/image/download/review/',
// };

// export default urls;
// const proxyUrl = 'https://cors-anywhere.herokuapp.com/';
// const proxyUrl = 'localhost:8082:8090/';
// const proxyUrl = 'localhost:8080/';
const proxyUrl = '';
const urls = {
    defaultUrl: proxyUrl + '127.0.0.1:8082/wallet',
    loginUrl: proxyUrl + '127.0.0.1:8082/wallet/api/authenticate/login',
    signupUrl:
        proxyUrl + '127.0.0.1:8082/wallet/api/authenticate/register',
    socketUrl: '127.0.0.1:8082/websocket',
    groupUrl: proxyUrl + '127.0.0.1:8082/wallet/api/gathering',
    profileUrl: proxyUrl + '127.0.0.1:8082/wallet/api/profile',
    topicSubscribeUrl: '/topic/gathering',
    sendMessageUrl: '/app/gathering',
	notificationUrl: '/user/queue/notification',
    reviewUrl: proxyUrl + '127.0.0.1:8082/wallet/api/review',
    picPreviewUrl: '127.0.0.1:8082/wallet/image/download/review/',
    avatarPreviewUrl: '127.0.0.1:8082/wallet/image/download/avatar/',
    uploadAvatarUrl: '127.0.0.1:8082/wallet/image/upload/avatar',
    
};

export default urls;
