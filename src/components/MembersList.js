import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import InboxIcon from '@material-ui/icons/Inbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import MemberItem from './MemberItem';
import axios from 'axios';
import urls from './Urls';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
}));

class MembersList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var listItems = this.props.members.map((member) => (
            <MemberItem
                firstName={member.firstName}
                lastName={member.lastName}
                username={member.username}
                avatar={member.avatar}
            ></MemberItem>
        ));
        return (
            <div>
                <List>{listItems}</List>
            </div>
        );
    }
}

export default MembersList;
