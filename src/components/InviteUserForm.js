import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { Component } from 'react';
import axios from 'axios';
import urls from './Urls';
import PersonAddRoundedIcon from '@material-ui/icons/PersonAddRounded';
import Tooltip from '@material-ui/core/Tooltip';

class InviteUserForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            token: this.props.token,
            identifier: '',
        };
    }
    handleCancel = () => {
        this.setState({ open: false });
    };

    handleClose = () => {
        const proxyurl = 'https://cors-anywhere.herokuapp.com/';
        axios
            .put(
                urls.groupUrl + '/' + this.props.groupId,
                {
                    identifier: this.state.identifier,
                },
                {
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: this.props.token,
                    },
                },
            )
            .then((res) => alert('Successfully invited'));
        this.setState({ open: false });
    };

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleChange = (e) => {
        this.setState({ identifier: e.target.value });
    };

    render() {
        return (
            <div>
                <Tooltip title="Invite a user to the group">
                    <IconButton
                        color="inherit"
                        disabled={this.state.selectedGroupId === ''}
                        onClick={this.handleClickOpen}
                    >
                        <PersonAddRoundedIcon></PersonAddRoundedIcon>
                    </IconButton>
                </Tooltip>

                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">
                        Invite User
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            To invite new user to selected group, please insert
                            user's email.
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="email"
                            label="Email"
                            type="email"
                            fullWidth
                            onChange={(e) => this.handleChange(e)}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCancel} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleClose} color="primary">
                            Update
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default InviteUserForm;
