import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import IconButton from '@material-ui/core/IconButton';
import { Component } from 'react';
import axios from 'axios';
import urls from './Urls';
import Avatar from '@material-ui/core/Avatar';
import Tooltip from '@material-ui/core/Tooltip';
import { store } from 'react-notifications-component';

class UpdateProfileForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            token: this.props.token,
            firstName: '',
            cFirstName: '',
            lastName: '',
            cLastName: '',
            username: '',
            cUsername: '',
            picture: null,
            avatar: '',
        };
    }

    handleCancel = () => {
        this.setState({ open: false });
    };

    handleClose = () => {
        console.log(this.state);
        const proxyurl = 'https://cors-anywhere.herokuapp.com/';
        axios
            .post(
                urls.profileUrl,
                {
                    firstName:
                        this.state.firstName === ''
                            ? this.state.cFirstName
                            : this.state.firstName,
                    lastName:
                        this.state.lastName === ''
                            ? this.state.cLastName
                            : this.state.lastName,
                    username:
                        this.state.username === ''
                            ? this.state.cUsername
                            : this.state.username,
                    avatar: this.state.avatar,
                },
                {
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: this.props.token,
                    },
                },
            )
            .then((res) => {
                store.addNotification({
                    title: 'Profile notice!',
                    message: 'your profile updated successfuly!',
                    type: 'success',
                    insert: 'top',
                    container: 'bottom-right',
                    animationIn: ['animated', 'fadeIn'],
                    animationOut: ['animated', 'fadeOut'],
                    dismiss: {
                        duration: 5000,
                        onScreen: true,
                    },
                });
            })
            .catch((res) => {
                store.addNotification({
                    title: 'Profile notice!',
                    message: 'Error occured while profile updating!',
                    type: 'danger',
                    insert: 'top',
                    container: 'bottom-right',
                    animationIn: ['animated', 'fadeIn'],
                    animationOut: ['animated', 'fadeOut'],
                    dismiss: {
                        duration: 5000,
                        onScreen: true,
                    },
                });
            });
        this.setState({ open: false });
    };

    handleClickOpen = () => {
        this.setState({ open: true });
        axios
            .get(urls.profileUrl, {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: this.state.token,
                    'Access-Control-Allow-Origin': 'http://127.0.0.1:3000',
                    'Access-Control-Allow-Methods': 'POST',
                    'Access-Control-Allow-Headers':
                        'Content-Type, Authorization',
                },
            })
            .then((res) => {
                var data = res.data;
                this.setState({
                    cFirstName: data.firstName,
                    cLastName: data.lastName,
                    cUsername: data.username,
                    avatar: data.avatar,
                });
            })
            .catch((res) => {
                console.log(res);
            });
    };

    handleFirstNameChange = (e) => {
        this.setState({ firstName: e.target.value });
    };

    handleLastNameChange = (e) => {
        this.setState({ lastName: e.target.value });
    };

    handleUserNameChange = (e) => {
        this.setState({ username: e.target.value });
    };

    onDrop = (picture) => {
        console.log(picture);
        this.setState({ picture: picture.target.files[0] });
        const formData = new FormData();
        formData.append('file', picture.target.files[0]);
        axios
            .post(urls.uploadAvatarUrl, formData, {
                headers: { 'Content-Type': 'multipart/form-data' },
            })
            .then((res) => {
                this.setState({ avatar: res.data.name });
                console.log(res.data.name);
            })
            .catch((error) => {
                console.log('shit');
            });
    };

    render() {
        return (
            <div>
                <Tooltip title="Update profile">
                    <IconButton color="inherit" onClick={this.handleClickOpen}>
                        <AccountCircleIcon></AccountCircleIcon>
                    </IconButton>
                </Tooltip>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    // aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">
                        Update Profile
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            To create new group, please choose a title for the
                            group
                        </DialogContentText>
                        <div
                            style={{
                                alignItems: 'center',
                                alignmentBaseline: 'central',
                            }}
                        >
                            <Avatar
                                alt="Remy Sharp"
                                style={{ width: '500', height: '500' }}
                                src={
                                    this.state.avatar === ''
                                        ? 'https://eu.ui-avatars.com/api/?name=' +
                                          'undefined undefined'
                                        : urls.avatarPreviewUrl +
                                          this.state.avatar
                                }
                            />
                        </div>
                        <div className="form-group files color">
                            <label>Upload Your File </label>
                            <input
                                type="file"
                                className="form-control"
                                name="file"
                                onChange={(e) => this.onDrop(e)}
                            />
                        </div>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="firstName"
                            label="First name"
                            defaultValue={this.props.firstName}
                            type="name"
                            fullWidth
                            helperText={
                                'Current: ' +
                                this.state.cFirstName +
                                ' ,leave it empty if you do not need to change it'
                            }
                            required={true}
                            onChange={(e) => this.handleFirstNameChange(e)}
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="lastName"
                            label="Last name"
                            defaultValue={this.props.lastName}
                            type="name"
                            fullWidth
                            helperText={
                                'Current: ' +
                                this.state.cLastName +
                                ' ,leave it empty if you do not need to change it'
                            }
                            onChange={(e) => this.handleLastNameChange(e)}
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="username"
                            label="Username"
                            defaultValue={this.props.user}
                            helperText={
                                'Current: ' +
                                this.state.cUsername +
                                ' ,leave it empty if you do not need to change it'
                            }
                            type="name"
                            fullWidth
                            onChange={(e) => this.handleUserNameChange(e)}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCancel} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleClose} color="primary">
                            Update
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default UpdateProfileForm;
