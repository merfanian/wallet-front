import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import RateReview from '@material-ui/icons/RateReview';
import IconButton from '@material-ui/core/IconButton';
import { Component } from 'react';
import Tooltip from '@material-ui/core/Tooltip';
import axios from 'axios';
import urls from './Urls';

class Review extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            openPreview: false,
            author: '',
            title: '',
            buyUrl: '',
            review: '',
            publisher: '',
            token: this.props.token,
            result: '',
        };
    }

    handleCancel = () => {
        this.setState({ open: false });
    };

    handlePreviewCancel = () => {
        this.setState({ openPreview: false });
    };

    handleClose = () => {
        const proxyurl = 'https://cors-anywhere.herokuapp.com/';
        axios
            .post(
                urls.reviewUrl,
                {
                    author: this.state.author,
                    title: this.state.title,
                    buyUrl: this.state.buyUrl,
                    review: this.state.review,
                    publisher: this.state.publisher,
                },
                {
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: this.props.token,
                    },
                },
            )
            .then((res) => {
                console.log(res.data);
                this.setState({ result: res.data.fileName });
                this.handlePreview();
            });
        this.setState({ open: false });
    };

    handlePreview = () => {
        this.setState({ open: false });
        this.setState({ openPreview: true });
    };

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleAuthorChange = (e) => {
        this.setState({ author: e.target.value });
    };

    handleTitleChange = (e) => {
        this.setState({ title: e.target.value });
    };

    handleBuyUrlChange = (e) => {
        this.setState({ buyUrl: e.target.value });
    };

    handleReviewChange = (e) => {
        this.setState({ review: e.target.value });
    };

    handlePublisherChange = (e) => {
        this.setState({ publisher: e.target.value });
    };

    render() {
        return (
            <div>
                <Tooltip title="New review">
                    <IconButton color="inherit" onClick={this.handleClickOpen}>
                        <RateReview></RateReview>
                    </IconButton>
                </Tooltip>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleCancel}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Add Review</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Fill the following fields to create a new review
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="title"
                            label="Title"
                            type="name"
                            fullWidth
                            onChange={(e) => this.handleTitleChange(e)}
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="author"
                            label="Author"
                            type="name"
                            fullWidth
                            onChange={(e) => this.handleAuthorChange(e)}
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="publisher"
                            label="Book Publisher"
                            type="name"
                            fullWidth
                            onChange={(e) => this.handlePublisherChange(e)}
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="buyUrl"
                            label="Link to the Book"
                            type="name"
                            fullWidth
                            onChange={(e) => this.handleBuyUrlChange(e)}
                        />
                        <TextField
                            autoFocus
                            multiline
                            rows={3}
                            rowsMax={5}
                            margin="dense"
                            id="review"
                            label="Review"
                            type="name"
                            fullWidth
                            onChange={(e) => this.handleReviewChange(e)}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCancel} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleClose} color="primary">
                            Add
                        </Button>
                    </DialogActions>
                </Dialog>
                <Dialog
                    open={this.state.openPreview}
                    onClose={this.handlePreviewCancel}
                    aria-labelledby="form-dialog-title1"
                >
                    <DialogTitle id="form-dialog-title1">
                        Review Preview
                    </DialogTitle>
                    <DialogContent>
                        <img
                            src={urls.picPreviewUrl + this.state.result}
                            alt="new"
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button
                            onClick={this.handlePreviewCancel}
                            color="primary"
                        >
                            CLose
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default Review;
