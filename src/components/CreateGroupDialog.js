import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import IconButton from '@material-ui/core/IconButton';
import { Component } from 'react';
import axios from 'axios';
import urls from './Urls';
import { Tooltip } from '@material-ui/core';
import { store } from 'react-notifications-component';
import Avatar from '@material-ui/core/Avatar';

class CreateGroupDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            token: this.props.token,
            groupName: '',
            picture: null,
            avatar: '',
        };
    }
    handleCancel = () => {
        this.setState({ open: false });
    };

    handleClose = () => {
        const proxyurl = 'https://cors-anywhere.herokuapp.com/';
        axios
            .post(
                urls.groupUrl,
                {
                    title: this.state.groupName,
                    avatar: this.state.avatar,
                },
                {
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: this.props.token,
                    },
                },
            )
            .then((res) => {
                store.addNotification({
                    title: 'Wonderful!',
                    message: 'Group ' + this.state.avatar + ' created!',
                    type: 'success',
                    insert: 'top',
                    container: 'bottom-right',
                    animationIn: ['animated', 'fadeIn'],
                    animationOut: ['animated', 'fadeOut'],
                    dismiss: {
                        duration: 5000,
                        onScreen: true,
                    },
                });
            })
            .finally(() => {
                var st = new Date().getTime() + 2000;
                while (new Date().getTime() < st);
                this.props.refreshGroups();
            });
        this.setState({ open: false });
    };

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleChange = (e) => {
        this.setState({ groupName: e.target.value });
    };

    onDrop = (picture) => {
        console.log(picture);
        this.setState({ picture: picture.target.files[0] });
        const formData = new FormData();
        formData.append('file', picture.target.files[0]);
        axios
            .post(urls.uploadAvatarUrl, formData, {
                headers: { 'Content-Type': 'multipart/form-data' },
            })
            .then((res) => {
                this.setState({ avatar: res.data.name });
                console.log(res.data.name);
            })
            .catch((error) => {
                console.log('shit');
            });
    };

    render() {
        return (
            <div>
                <Tooltip title="Create a group">
                    <IconButton color="inherit" onClick={this.handleClickOpen}>
                        <GroupAddIcon></GroupAddIcon>
                    </IconButton>
                </Tooltip>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">
                        Create Group
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            To create new group, please choose a title for the
                            group
                        </DialogContentText>
                        <div
                            style={{
                                alignItems: 'center',
                                alignmentBaseline: 'central',
                            }}
                        >
                            <Avatar
                                alt="Remy Sharp"
                                style={{ width: '500', height: '500' }}
                                src={
                                    this.state.avatar === ''
                                        ? 'https://eu.ui-avatars.com/api/?name=' +
                                          'undefined undefined'
                                        : urls.avatarPreviewUrl +
                                          this.state.avatar
                                }
                            />
                        </div>
                        <div className="form-group files color">
                            <label>Upload Your File </label>
                            <input
                                type="file"
                                className="form-control"
                                name="file"
                                onChange={(e) => this.onDrop(e)}
                            />
                        </div>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Title"
                            type="name"
                            fullWidth
                            onChange={(e) => this.handleChange(e)}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCancel} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleClose} color="primary">
                            Create
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default CreateGroupDialog;
