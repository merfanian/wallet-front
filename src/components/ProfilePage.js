import React, { Component } from 'react';
import GroupsBar from './GroupsBar';
import axios from 'axios';
import urls from './Urls';
import { Container, Row, Col } from 'react-grid-system';
import MembersList from './MembersList';
import ClipLoader from 'react-spinners/ClipLoader';
import { css } from '@emotion/core';

const override = css`
    display: flex;
    justifyContent: "center",
    alignItems: "center"
    border-color: blue;
`;

class ProfilePage extends Component {
    constructor(props) {
        super(props);
        this.handleGroupSelect.bind(this);
        this.state = {
            groups: [],
            firstName: '',
            lastName: '',
            token: this.props.token,
            groupMembers: [],
            selectedGroupId: '',
            groupsAreLoading: true,
            membersAreLoading: false,
        };
    }

    handleGroupSelect(group) {
        this.setState({
            selectedGroupId: group.props.id,
            membersAreLoading: true,
        });
        const proxyurl = 'https://cors-anywhere.herokuapp.com/';
        axios
            .get(proxyurl + urls.groupUrl + '/' + this.state.selectedGroupId, {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: this.state.token,
                },
            })
            .then((res) => {
                this.setState({ groupMembers: res.data });
                this.setState({ membersAreLoading: false });
            })
            .catch((res) => {
                this.setState({ groupMembers: [] });
                console.log(res);
            });
    }

    componentDidMount() {
        const proxyurl = 'https://cors-anywhere.herokuapp.com/';
        axios
            .get(proxyurl + urls.groupUrl, {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: this.state.token,
                },
            })
            .then((res) => {
                this.setState({ groups: res.data });
                this.setState({ groupsAreLoading: false });
            })
            .catch((res) => {
                this.setState({ groups: '' });
            });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col sm={4}>
                        <div>
                            <ClipLoader
                                size={10}
                                css={override}
                                color={'#123abc'}
                                loading={this.state.groupsAreLoading}
                            />
                        </div>
                        <GroupsBar
                            groups={this.state.groups}
                            handleGroupSelect={(e) => this.handleGroupSelect(e)}
                        ></GroupsBar>
                    </Col>
                    <Col sm={4}>
                        {this.state.membersAreLoading ? (
                            <div>
                                <ClipLoader
                                    size={10}
                                    css={override}
                                    color={'#123abc'}
                                    loading={this.state.membersAreLoading}
                                />
                            </div>
                        ) : (
                            <MembersList
                                members={this.state.groupMembers}
                            ></MembersList>
                        )}
                    </Col>
                    <Col sm={4}>three of three columns</Col>
                </Row>
            </Container>
        );
    }
}

export default ProfilePage;
