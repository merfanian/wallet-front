import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Collections from '@material-ui/icons/Collections';
import IconButton from '@material-ui/core/IconButton';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import urls from './Urls';
import ReviewCard from './ReviewCard';
import Slide from '@material-ui/core/Slide';
import GridList from '@material-ui/core/GridList';
import Tooltip from '@material-ui/core/Tooltip';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

class ReviewPack extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            reviewsAreLoading: true,
            token: this.props.token,
            func: this.props.func,
            reviews: [],
        };
    }

    handleCancel = () => {
        this.setState({ open: false });
    };

    handleClickOpen = () => {
        const proxyurl = 'https://cors-anywhere.herokuapp.com/';
        axios
            .get(urls.reviewUrl, {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: this.state.token,
                    'Access-Control-Allow-Origin': 'http://127.0.0.1:3000',
                    'Access-Control-Allow-Methods': 'POST',
                    'Access-Control-Allow-Headers':
                        'Content-Type, Authorization',
                },
            })
            .then((res) => {
                this.setState({ reviews: res.data });
                console.log(this.state.reviews);
                this.setState({ reviewsAreLoading: false });
            });
        this.setState({ open: true });
    };

    render() {
        var listItems = this.state.reviews.map((review) => (
            <ReviewCard
                url={urls.picPreviewUrl + review.fileName}
                fileName={review.fileName}
                title={review.title}
                author={review.author}
                func={this.state.func}
                cancel={this.handleCancel}
            ></ReviewCard>
        ));
        console.log(this.state.groupID);
        return (
            <div>
                <Tooltip title="My reviews">
                    <IconButton color="inherit" onClick={this.handleClickOpen}>
                        <Collections></Collections>
                    </IconButton>
                </Tooltip>
                <Dialog
                    fullScreen
                    TransitionComponent={Transition}
                    open={this.state.open}
                    onClose={this.handleCancel}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle
                        id="form-dialog-title"
                        style={{ backgroundColor:this.props.darkMode ? '#110022' : '#e8efff', color:this.props.darkMode ? 'white' : 'black'}}
                    >
                        My Reviews
                    </DialogTitle>
                    <DialogContent style={{ backgroundColor: this.props.darkMode ? '#110022' : '#e8efff' }}>
                        <DialogContentText style={{color:this.props.darkMode ? 'white' : 'black'}}>
                            {this.state.reviewsAreLoading ? 'Loading...' : null}
                        </DialogContentText>
                        <GridList children={listItems}></GridList>
                    </DialogContent>
                    <DialogActions style={{ backgroundColor: this.props.darkMode ? '#110022' : '#e8efff' }}>
                        <Button onClick={this.handleCancel} style={{color:this.props.darkMode ? 'white' : 'primary'}}>
                            Close
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default ReviewPack;
