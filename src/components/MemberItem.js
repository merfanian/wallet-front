import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import InboxIcon from '@material-ui/icons/Inbox';
import FaceRoundedIcon from '@material-ui/icons/FaceRounded';
import DraftsIcon from '@material-ui/icons/Drafts';
import urls from './Urls';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
}));

class MemberItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const src = this.props.avatar
            ? urls.avatarPreviewUrl + this.props.avatar
            : 'https://eu.ui-avatars.com/api/?uppercase=false&name=' +
              this.props.firstName +
              '+' +
              this.props.lastName +
              '&rounded=true&size=48';
        return (
            <ListItem>
                <ListItemIcon cstyle={{ color: 'white' }}>
                    <img
                        src={src}
                        style={{
                            height: '48px',
                            width: '48px',
                            borderRadius: '24px',
                        }}
                    ></img>
                </ListItemIcon>
                <ListItemText
                    color="white"
                    style={{ color: 'white' }}
                    primary={this.props.firstName + ' ' + this.props.lastName}
                />
            </ListItem>
        );
    }
}

export default MemberItem;
