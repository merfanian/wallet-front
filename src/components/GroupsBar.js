import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import InboxIcon from '@material-ui/icons/Inbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import GroupItem from './GroupItem';
import axios from 'axios';
import urls from './Urls';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
}));

class GroupsBar extends Component {
    constructor(props) {
        super(props);
        this.handleGroupClick.bind(this);

        this.state = {
            groups: this.props.groups,
            groupMembers: [],
            selectedIndex: '-1',
            listItems: this.listItems,
        };
    }

    handleGroupClick(group) {
        this.setState({ selectedIndex: group.props.id });
        this.props.handleGroupSelect(group);
    }

    render() {
        var listItems = this.props.groups.map((group) => (
            <GroupItem
                color={this.props.color}
                title={group.title}
                id={group.id}
                handleGroupClick={this.handleGroupClick.bind(this)}
                selected={group.id === this.state.selectedIndex}
                groups={this.props.groups}
                counter={group.counter}
                avatar={group.avatar}
            ></GroupItem>
        ));
        return (
            <div>
                <List>{listItems}</List>
            </div>
        );
    }
}

export default GroupsBar;
